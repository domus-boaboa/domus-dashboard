package org.boaboa.dashboard.arduino;

public enum TypeConnectionEnum {

	DW("Digital Write"), AR("Analog Read");

	private TypeConnectionEnum(String description) {
		this.description = description;
	}

	private String description;

	public String getDescription() {
		return description;
	}

}
