package org.boaboa.dashboard.controller;

import java.util.List;
import java.util.Map;

import org.boaboa.dashboard.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@CrossOrigin(origins = "*")
public class TypeController {
	
	@Autowired
	private TypeService _typeService;

	@RequestMapping(path = "/types", method = RequestMethod.GET)
	public ResponseEntity<List<Map<String, String>>> allTypes(){
		List<Map<String, String>> types = _typeService.getAll();
		return new ResponseEntity<List<Map<String, String>>>(types, HttpStatus.OK);
		
	}
	

}
