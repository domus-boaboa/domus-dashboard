package org.boaboa.dashboard.controller;

import org.boaboa.dashboard.document.DashboardDocument;
import org.boaboa.dashboard.document.DeviceDocument;
import org.boaboa.dashboard.exception.DeviceException;
import org.boaboa.dashboard.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/dashboard")
@CrossOrigin(origins = "*")
@Controller
public class DashboardController {

	@Autowired
	private DashboardService _dashboardService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<DashboardDocument> addDashbard(@Validated @RequestBody DashboardDocument dashboard) {
		DashboardDocument newDashboard = _dashboardService.addDashboard(dashboard);
		return new ResponseEntity<DashboardDocument>(newDashboard, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<DashboardDocument> getDashbard() {
		DashboardDocument dashboard = _dashboardService.getDashboard();
		return new ResponseEntity<DashboardDocument>(dashboard, HttpStatus.OK);
	}

	@RequestMapping(path = "/device", method = RequestMethod.POST)
	public ResponseEntity<DeviceDocument> addDevice(@Validated @RequestBody DeviceDocument device) throws DeviceException {
		DeviceDocument newDevice;
		newDevice = _dashboardService.addDevice(device);
		return new ResponseEntity<DeviceDocument>(newDevice, HttpStatus.CREATED);
	}

	@RequestMapping(path = "/device/{name}", method = RequestMethod.DELETE)
	public ResponseEntity<DeviceDocument> deleteDevice(@PathVariable String name) throws DeviceException {
		DeviceDocument device;
		device = _dashboardService.deleteDevice(name);
		return new ResponseEntity<DeviceDocument>(device, HttpStatus.ACCEPTED);
	}
}
