package org.boaboa.dashboard.service;

import java.util.ArrayList;
import java.util.List;

import org.boaboa.dashboard.document.DashboardDocument;
import org.boaboa.dashboard.document.DeviceDocument;
import org.boaboa.dashboard.exception.DeviceException;
import org.boaboa.dashboard.repository.DashboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private DashboardRepository _dashboardRepository;

	@Override
	public DashboardDocument getDashboard() {
		return _dashboardRepository.findAll().get(0);
	}

	@Override
	public DashboardDocument addDashboard(DashboardDocument newDashboard) {
		newDashboard.setDevices(new ArrayList<>());
		DashboardDocument dashboard = _dashboardRepository.save(newDashboard);
		return dashboard;
	}

	@Override
	public DeviceDocument addDevice(DeviceDocument device) throws DeviceException {
		DashboardDocument dashboardDocument = _dashboardRepository.findAll().get(0);
		List<DeviceDocument> devices = dashboardDocument.getDevices();
		for (int i = 0; i < devices.size(); i++) {
			if (device.getName().equals(devices.get(i).getName())) {
				throw new DeviceException("Ese nombre ya existe!");
			}
		}
		devices.add(device);
		dashboardDocument.setDevices(devices);
		_dashboardRepository.save(dashboardDocument);
		//TODO: Enviar notificación a los demás servicios!
		return device;
	}

	@Override
	public DeviceDocument deleteDevice(String name) throws DeviceException {
		DashboardDocument dashboard = _dashboardRepository.findAll().get(0);
		List<DeviceDocument> devices = dashboard.getDevices();
		DeviceDocument findDevice = null;
		for (DeviceDocument device : devices) {
			if (device.getName().equals(name)) {
				findDevice = device;
				devices.remove(device);
				break;
			}
		}
		if (findDevice != null) {
			dashboard.setDevices(devices);
			_dashboardRepository.save(dashboard);
		}else {
			throw new DeviceException("No se encontro el dispositivo");
		}
		//TODO: Enviar notificación a los demás servicios!
		return findDevice;
	}

}
