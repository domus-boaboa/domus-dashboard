package org.boaboa.dashboard.service;

import java.util.List;
import java.util.Map;

public interface TypeService {

	List<Map<String, String>> getAll();

}
