package org.boaboa.dashboard.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.boaboa.dashboard.arduino.TypeConnectionEnum;
import org.springframework.stereotype.Service;

@Service
public class TypeServiceImp implements TypeService {

	@Override
	public List<Map<String, String>> getAll() {
		TypeConnectionEnum[] values = TypeConnectionEnum.values();
		List<Map<String, String>> options = new ArrayList<>();
		for (TypeConnectionEnum typeConnectionEnum : values) {
			Map<String,String> hashMap = new HashMap<>();
			hashMap.put("value", typeConnectionEnum.name());
			hashMap.put("text", typeConnectionEnum.getDescription());
			options.add(hashMap);
		}
		return options;
	}

}
