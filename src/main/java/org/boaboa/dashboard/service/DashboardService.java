package org.boaboa.dashboard.service;

import org.boaboa.dashboard.document.DashboardDocument;
import org.boaboa.dashboard.document.DeviceDocument;
import org.boaboa.dashboard.exception.DeviceException;

public interface DashboardService {
	
	DashboardDocument getDashboard();

	DashboardDocument addDashboard(DashboardDocument newDashboard);

	DeviceDocument addDevice(DeviceDocument device) throws DeviceException;

	DeviceDocument deleteDevice(String name) throws DeviceException;

}
