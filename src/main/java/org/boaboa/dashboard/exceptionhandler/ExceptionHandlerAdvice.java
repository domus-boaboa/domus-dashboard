package org.boaboa.dashboard.exceptionhandler;

import java.util.HashMap;
import java.util.Map;

import org.boaboa.dashboard.exception.DeviceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerAdvice {
	
	@ExceptionHandler(DeviceException.class)
    public ResponseEntity<DashboardException> handleException(DeviceException e) {
        DashboardException body = new DashboardException();
        body.setCode(500);
        body.setMessage(e.getMessage());
		return new ResponseEntity<DashboardException>(body, HttpStatus.NOT_ACCEPTABLE);
    }
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, ValidationField>> handleException(MethodArgumentNotValidException e){
		Map<String, ValidationField> map = new HashMap<>();
		ValidationField vaField = null;
		for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
			vaField = new ValidationField();
			vaField.setField(fieldError.getField());
			vaField.setMessage(fieldError.getDefaultMessage());
			vaField.setError(fieldError.getCode());
			vaField.setValue(fieldError.getRejectedValue());
			map.put(fieldError.getField(), vaField);
		}
		return new ResponseEntity<Map<String, ValidationField>>(map, HttpStatus.BAD_REQUEST);
	}

}
