package org.boaboa.dashboard.repository;

import org.boaboa.dashboard.document.DashboardDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardRepository extends MongoRepository<DashboardDocument, String> {

}
