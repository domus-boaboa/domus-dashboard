package org.boaboa.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomusDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomusDashboardApplication.class, args);
	}
}
