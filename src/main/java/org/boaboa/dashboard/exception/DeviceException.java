package org.boaboa.dashboard.exception;

public class DeviceException extends Exception {
	
	public DeviceException() {
		super();
	}

	public DeviceException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2131840652731670950L;

}
