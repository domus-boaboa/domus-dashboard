package org.boaboa.dashboard.document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.boaboa.dashboard.arduino.TypeConnectionEnum;

public class DeviceDocument {

	@NotNull
	@Size(min = 1, max = 3)
	private String pin;

	@NotNull
	@Size(min = 3, max = 10)
	private String icon;

	@NotNull
	@Size(min = 3, max = 10)
	private String name;

	@NotNull
	private TypeConnectionEnum type;

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TypeConnectionEnum getType() {
		return type;
	}

	public void setType(TypeConnectionEnum type) {
		this.type = type;
	}

}
