package org.boaboa.dashboard.document;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;

public class DashboardDocument {
	
	@Id
	private String id;

	@Size(min = 4, max = 30)
	@NotNull
	private String name;
	
	private List<DeviceDocument> devices;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DeviceDocument> getDevices() {
		return devices;
	}

	public void setDevices(List<DeviceDocument> devices) {
		this.devices = devices;
	}

}
